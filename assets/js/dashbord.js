$(function(){
	$('.dropdown-link').on('click',function(e){
		e.preventDefault();
		$(this).parent().toggleClass('active-link');
		$(this).parent().find('.dropdown-menu').slideToggle(500);
		
	});
    ////////////// form_control file input ////////////
    $('form .form-group input[type="file"]').wrap('<div class="custom_file"></div>');
    $('.custom_file').prepend('<span class="file-path" >Upload Your File</span>');
    $('.custom_file').append('<i class="fa fa-upload"></i>');
    $('form .form-group input[type="file"]').change(function(){
        var trim = $(this).val().substr(0,50);
        $(this).prev('span').text(trim);
    });
    //############ create clock ###############
    $('.clock').on('click',function(){
        $(this).css({
            right:"-300px"
        });
    });
    // Create two variable with the names of the months and days in an array
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
    var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    // Create a newDate() object
    var newDate = new Date();
    // Extract the current date from Date object
    newDate.setDate(newDate.getDate());
    // Output the day, date, month and year    
    $('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());
    
    setInterval( function() {
        // Create a newDate() object and extract the seconds of the current time on the visitor's
        var seconds = new Date().getSeconds();
        // Add a leading zero to seconds value
        $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
        },1000);
        
    setInterval( function() {
        // Create a newDate() object and extract the minutes of the current time on the visitor's
        var minutes = new Date().getMinutes();
        // Add a leading zero to the minutes value
        $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
        },1000);
        
    setInterval( function() {
        // Create a newDate() object and extract the hours of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to the hours value
        $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
        }, 1000);
        
    //######### layout color   ############
    $("#site-color").change(function() {
        var color = $("#site-color").val();
        var rgbaCol = 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',' + .5 + ')';
    
        $('.layout').css('background-color', rgbaCol)
    });
    //######### change img   ############
    $("#bg-img").change(function() {
        // console.log($(this).val());
        ////////////////
        var files = !!this.files ? this.files : [];
        if ( !files.length || !window.FileReader ) return;
        if ( /^image/.test( files[0].type ) ) {
            var reader = new FileReader();
            reader.readAsDataURL( files[0] );
            reader.onloadend = function(){
                $('.custom_file').find('.upload-img').remove();
                $('.custom_file').prepend("<img src=" + this.result + " class='upload-img'>");
                $(".side-bar").css("background-image", "url(" + this.result + ")");
            }
        }
        ////////////
    });
    //######## bg-img-status ########
    $('#bg-img-status').on('click',function(){
        if((this).checked){
            $(".side-bar").css("background-image", '');
            //console.log($(this).val());
        }else{
            $(".side-bar").css("background-image", "none");
        }
    });
}); 
